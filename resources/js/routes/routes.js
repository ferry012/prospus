import store from '../store';


let App = () => import(/* webpackChunkName: "home" */'./../components/App');
let Home = () => import(/* webpackChunkName: "home" */'../views/Home');
let Login = () => import(/* webpackChunkName: "login" */'../views/Login');
let Register = () => import(/* webpackChunkName: "register" */'../views/Register');

let Profile = () => import(/* webpackChunkName: "profile" */'../views/profile/Profile');


/*permissions*/
let PermissionIndex = () => import(/* webpackChunkName: "permission" */'../views/permissions/PermissionIndex');
let PermissionTable = () => import(/* webpackChunkName: "permission" */'../views/permissions/PermissionTable');
let PermissionCreate = () => import(/* webpackChunkName: "permission" */'../views/permissions/PermissionCreate');

/*roles*/
let RoleIndex = () => import(/* webpackChunkName: "role" */'../views/roles/RoleIndex');
let RoleTable = () => import(/* webpackChunkName: "role" */'../views/roles/RoleTable');

/*users*/
let UserIndex = () => import(/* webpackChunkName: "user" */'../views/users/UserIndex');
let UserTable = () => import(/* webpackChunkName: "user" */'../views/users/UserTable');


/*error views*/
let Error_404 = () => import(/* webpackChunkName: "error_view" */'./../views/error_view/Error_404');

const routes = [
    {
        path: "",
        redirect: {name: 'home'},
        component: App,
        children: [
            {
                path: "/home",
                name: "home",
                component: Home,
                meta: {
                    title: 'Home',
                    requiresAuth: true,
                },

            },

            {
                path: "/profile",
                name: "profile",
                component: Profile,
                meta: {
                    title: 'Profile',
                    requiresAuth: true,
                    breadCrumbs: [
                        {
                            to: {name: 'home'},           // hyperlink
                            text: 'Home', // crumb text
                        },
                        {
                        to: {name: 'profile'},           // hyperlink
                        text: 'Profile', // crumb text
                        active:true
                    }]
                },

            },

            {
                path: "/permissions",
                name: "permissionIndex",
                redirect: {name: 'permissionTable'},
                component: PermissionIndex,
                children: [
                    {
                        path: "/",
                        name: "permissionTable",
                        component: PermissionTable,
                        meta: {
                            title: 'Permissions',
                            requiresAuth: true,
                            permissions: ['permission_view'],
                            breadCrumbs: [
                                {
                                    to: {name: 'home'},           // hyperlink
                                    text: 'Home', // crumb text,
                                    icon:'house-fill'
                                },
                                {
                                to: {name: 'permissionIndex'},           // hyperlink
                                text: 'Permissions', // crumb text
                                active:true
                            }]
                        },
                    },
                    {
                        path: "create",
                        name: "permissionCreate",
                        component: PermissionCreate,
                        meta: {
                            title: 'Permissions',
                            requiresAuth: true,
                            permissions: ['permission_view'],
                            breadCrumbs: [
                                {
                                    to: {name: 'home'},           // hyperlink
                                    text: 'Home', // crumb text
                                },
                                {
                                to: {name: 'permissionIndex'},           // hyperlink
                                text: 'Permissions' // crumb text
                            },
                                {
                                    to: {name: 'permissionCreate'},           // hyperlink
                                    text: 'Create Permission', // crumb text,
                                    active: true
                                },

                            ]
                        },
                    },
                ]

            },
            {
                path: "/roles",
                name: "roleIndex",
                redirect: {name: 'roleTable'},
                component: RoleIndex,
                children: [
                    {
                        path: "/",
                        name: "roleTable",
                        component: RoleTable,
                        meta: {
                            title: 'Roles',
                            requiresAuth: true,
                            permissions: ['role_view'],
                            breadCrumbs: [ {
                                to: {name: 'home'},           // hyperlink
                                text: 'Home', // crumb text
                            },
                                {
                                to: {name: 'roleIndex'},          // hyperlink
                                text: 'Roles', // crumb text
                                active:true
                            }]
                        },
                    },
                ]

            },
            {
                path: "/users",
                name: "userIndex",
                redirect: {name: 'userTable'},
                component: UserIndex,
                children: [
                    {
                        path: "/",
                        name: "userTable",
                        component: UserTable,
                        meta: {
                            title: 'Users',
                            requiresAuth: true,
                            permissions: ['user_view'],
                            breadCrumbs: [
                                {
                                    to: {name: 'home'},           // hyperlink
                                    text: 'Home', // crumb text
                                },
                                {
                                to: {name: 'userIndex'},      // hyperlink
                                text: 'Users', // crumb text
                                active:true
                            }]
                        },
                    },
                ]

            },
        ]
    },
    {
        path: "/login",
        name: "login",
        component: Login,
        meta: {
            title: 'Login',
            requiresAuth: false,
        },
    },
    {
        path: "/register",
        name: "register",
        component: Register,
        meta: {
            title: 'Register',
            requiresAuth: false,
        },
    },
    {
        path: "*",
        name: "error_404",
        component: Error_404,
        meta: {
            requiresAuth: false,
        }
    }
];

export default routes;
