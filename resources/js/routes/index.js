import VueRouter from 'vue-router';
import Vue from 'vue';
import routes from './routes';
import store from '../store'
Vue.use(VueRouter);



const router = new VueRouter({
    routes,
    mode: 'history',
    linkActiveClass: "active",
    linkExactActiveClass: "active",
});


router.beforeEach( async (to, from, next) => {
    console.log('JAY INDEX123', to,to.matched.some(record => record.meta.requiresAuth));
    document.title = to.meta.title + ' | '+ process.env.MIX_APP_NAME;
    store.commit('page/PAGE_NAME',to.meta.title)
    if (to.matched.some(record => record.meta.requiresAuth)) {
        console.log('TRUE');
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!store.getters['auth/check']) {
            next({
                path: '/login',
                query: {redirect: to.fullPath}
            })
        } else {

            //if no role present
            //const permissions = store.state.auth.permissions;
            const permissions = (typeof store.state.auth.permissions == "object" ? store.state.auth.permissions : JSON.parse(store.state.auth.permissions)) || [];
            const roles  = (typeof store.state.auth.roles == "object" ? store.state.auth.roles : JSON.parse(store.state.auth.roles)) || [];
            if(roles.includes('super_administrator')){
                //super_administrator
               return next();

            } else if(!to.meta.roles){

                if(!to.meta.permissions){
                    return next();
                }else{
                    if (permissions.some(r => to.meta.permissions.includes(r))) {
                        next();
                    } else {
                        next('/404');
                    }
                }

            }else{

                if (roles.some(r => to.meta.roles.includes(r))) {
                    next();
                } else {
                    next('/404');
                }
            }
        }
    }else{
        console.log('FALSE');
        console.log('JAY INDEX FALSE',to.path,store.getters['auth/check']);
        if (to.path === '/login' && store.getters['auth/check']) {
            console.log('JAY INDEX FALSE IF');
            next({
                name: 'home',
            })
        } else {
            console.log('JAY INDEX FALSE ELSE');
            next()
        }

    }
});

export default router;
