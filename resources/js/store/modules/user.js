import axios from 'axios';
import * as types from '../mutation-types'
import {Form} from "vform";


const state = {
    users: {},
    all_user: [],
    page: {},
    search: '',
    sortBy: 'created_at',
    sortDir: 'desc',
    perPage: 5,
    user: new Form({
        name: null,
        username:null,
        email:null,
        password:null,
        password_confirmation:null,
        roles:[],
        birthday:null,
    }),
    user_id: null,

};
const getters = {
    users: state => state.users,
    all_user: state => state.all_user,
    page: state => state.page,
    search: state => state.search,
    sortBy: state => state.sortBy,
    sortDir: state => state.sortDir,
    perPage: state => state.perPage,
    user: state => state.user,
    user_id: state => state.user_id,
};

const actions = {
    async getUser({commit, state}) {
        try {
            const uri = `/api/users/${state.user_id}/edit`;
            const {data} = await axios.get(uri);
            commit(types.USER, data.data)
        } catch (e) {
            console.log(e);
        }
    },
    async fetchUsersForDatatable({commit, state}) {
        try {
            const uri = '/api/users?page=' + state.page + '&search=' + state.search + '&sortby=' + state.sortBy + '&sortdir=' + state.sortDir + '&currentpage=' + state.perPage;
            const {data} = await axios.get(uri);
            commit(types.USERS, data)

        } catch (e) {
            console.log(e);
        }
    },

    async getUsers({commit, state}) {
        try {
            const uri = `/api/users?getAll=true`;
            const {data} = await axios.get(uri);
            commit(types.ALL_USER, data.data)
        } catch (e) {
            console.log(e);
        }
    },

    createUser({commit, state}) {
        return new Promise((res, rej) => {
            state.user.post('/api/users')
                .then((response) => {
                    commit(types.USER_CLEAR)
                    res(response.data);
                }).catch((err) => {
                rej(err.response)
            })
        })
    },
    updateUser({commit, state}) {
        return new Promise((res, rej) => {
            state.user.patch(`/api/users/${state.user_id}`)
                .then((response) => {
                    commit(types.USER_CLEAR)
                    res(response.data);
                }).catch((err) => {
                rej(err.response)
            })
        })
    },

};

const mutations = {
    [types.USER_CLEAR](state) {
        state.user.clear();
        state.user.reset();
    },
    [types.USERS](state, users) {
        state.users = users
    },
    [types.USER](state, user) {
        state.user.fill(user)
    },
    [types.USER_PAGE](state, page) {
        state.page = page
    },
    [types.USER_SEARCH](state, search) {
        state.search = search
    },
    [types.USER_SORTBY](state, sortBy) {
        state.sortBy = sortBy
    },
    [types.USER_SORTDIR](state, sortDir) {
        state.sortDir = sortDir
    },
    [types.USER_PERPAGE](state, perPage) {
        state.perPage = perPage
    },
    [types.USER_CREATE](state, user) {
        state.user = user
    },
    [types.USER_UPDATE](state, user) {
        state.user  = user
    },
    [types.USER_ID](state, id) {
        state.user_id = id
    },
    [types.ALL_USER](state, users) {
        state.all_user = users
    },


};

export {
    state,
    getters,
    actions,
    mutations
};
