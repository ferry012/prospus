import axios from 'axios';
import * as types from '../mutation-types'
import {Form} from "vform";
import {REGISTER_USER, REGISTER_USER_CLEAR, REGISTER_USER_CREATE} from "../mutation-types";


const state = {
    user: new Form({
        name: null,
        username:null,
        email:null,
        password:null,
        password_confirmation:null,
        birthday:'1996-01-01'
        // roles:[],
    }),
};
const getters = {
    user: state => state.user,
};

const actions = {
    createUser({commit, state}) {
        return new Promise((res, rej) => {
            state.user.post('/api/register')
                .then((response) => {
                    commit(types.REGISTER_USER_CLEAR)
                    res(response.data);
                }).catch((err) => {
                rej(err.response)
            })
        })
    },
};

const mutations = {
    [types.REGISTER_USER_CLEAR](state) {
        state.user.clear();
        state.user.reset();
    },
    [types.REGISTER_USER_CREATE](state, user) {
        state.user = user
    },
};

export {
    state,
    getters,
    actions,
    mutations
};
