import * as types from '../mutation-types'


const state = {
    pageName: null,
};

const getters ={
    pageName: state => state.pageName
};

const actions = {

};

const mutations = {


    [types.PAGE_NAME] (state,page) {
        state.pageName = page;
    },


};

export  {
    state,
    getters,
    actions,
    mutations
};
